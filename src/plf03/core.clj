(ns plf03.core)

(defn funcion-comp-1
  []
  (let [fu (fn [x] (inc x))
        gu (fn [x] (* 2 x))
        h (comp gu fu)]
   (h 12)))

(defn funcion-comp-2
  []
  (let [fu (fn [x] (* 2 x))
        gu (fn [x] (inc x))
        h (comp fu gu)]
    (h 6)))
(defn funcion-comp-3
  []
  (let [fu (fn [x] (inc x))
        gu (fn [x] (+ 5 x))
        i (comp gu fu)]
    (i 10)))

(defn funcion-comp-4
  []
  (let [fu (fn [x] (+ 10 x))
        gu (fn [x] (* 2 x))
        hu (fn [x] (inc x))
        i (comp fu gu hu)]
    (i 100)))

(defn funcion-comp-5
  []
  (let [fu (fn [x] (/ 10 x))
        gu (fn [x] (* 2 x))
        hu (fn [x] (inc x))
        ju (fn [x] (inc x))
        i (comp fu gu hu ju)]
    (i 30)))
(defn funcion-comp-6
  []
   (let [fu (fn [x] (inc x))
         gu (fn [x] (* 2 x))
         hu (fn [x] (- 10 x))
         i (comp fu gu hu)]
     (i 10)))

(defn funcion-comp-7
  []
  (let [fu (fn [x] (inc x))
        gu (fn [x] (+ 10 x))
        i (comp fu gu fu gu gu)]
    (i 1)))

(defn funcion-comp-8
  []
  (let [fu (fn [x] (inc x))
        gu (fn [x] (+ 2 x))
        hu (fn [x] (inc x))
        i (comp fu gu hu)]
    (fu (gu 10))))

(defn funcion-comp-9
  []
  (let [fu (fn [xs] (filter even? xs))
        gu (fn [xs] (map inc xs))
        i (comp fu gu)]
    (i [1 3 4])))

(defn funcion-comp-10
  []
  (let [fu (fn [xs] (filter integer? xs))
        gu (fn [xs] (map inc xs))
        i (comp gu fu)]
    (i [1 2.5 3 4 5 6.123 7.9])))

(defn funcion-comp-11
  []
  (let [fu (fn [x] (take 3 x))
        gu (fn [x] (map inc x))
        i (comp fu gu)]
    (i [2 4 6 8 10])))

(defn funcion-comp-12
  []
  (let [fu (fn [xs] (map pos-int? xs))
        gu (fn [xs] (take 4 xs))
        hu (fn [xs] (map inc xs))
        i (comp fu gu hu)]
    (i [-1 2 -10 12 23 -0.9])))

(defn funcion-comp-13
  []
  (let [fu (fn [xs] (take 3 xs))
        gu (fn [xs] (map char? xs ))
        i (comp fu gu)]
    (i [\q "a" \v "dc"])))

(defn funcion-comp-14
  []
  (let [fu (fn [xs] (map inc xs))
        gu (fn [xs] (filter number? xs))
        i (comp fu gu)]
    (i [3 1 \w "af" 23 :a 90 2 1])))

(defn funcion-comp-15
  []
  (let [hu (fn [xs] (first xs))
        fu (fn [xs] (map inc xs))
        gu (fn [xs] (filter number? xs))
        i (comp hu fu gu)]
    (i [3 1 \w "af" 23 :a 90 2 1])))

(defn funcion-comp-16
  []
  (let [hu (fn [xs] (drop 2 xs))
        fu (fn [xs] (map inc xs))
        gu (fn [xs] (filter number? xs))
        i (comp hu fu gu)]
    (i [3 1 \w "af" 23 :a 90 2 1])))

(defn funcion-comp-17
  []
  (let [
        hu (fn [xs] (take-last 2 xs))
        fu (fn [xs] (map inc xs))
        gu (fn [xs] (filter number? xs))
        i (comp hu fu gu)]
    (i [3 1 \w "af" 23 :a 90 2 1])))

(defn funcion-comp-18
  []
  (let [hu (fn [xs] (group-by even? xs))
        fu (fn [xs] (map inc xs))
        gu (fn [xs] (filter number? xs))
        i (comp hu fu gu)]
    (i [3 1 \w "af" 23 :a 90 2 1])))

(defn funcion-comp-19
  []
  (let [fu (fn [xs] (remove pos? xs))
        gu (fn [xs] (map inc xs))
        hu (fn [xs] (filter number? xs))
        i (comp fu gu hu)]
    (i [3 -4 "a" 8 \q -10 -2.1 31])))

(defn funcion-comp-20
  []
  (let [fu (fn [xs] (remove pos? xs))
        gu (fn [xs] (map dec xs))
        hu (fn [xs] (filter number? xs))
        i (comp fu gu hu)]
    (i [3 -4 "a" 8 \q -10 -2.1 31])))

(defn funcion-complement-1
  []
  (let [fu (fn [x] (even? x))
        i (complement fu)]
    (i 20)))

(defn funcion-complement-2
  []
  (let [fu (fn [x] (char? x))
        i (complement fu)]
    (i \q)))

(defn funcion-complement-3
  []
  (let [fu (fn [x] (pos? x))
        i (complement fu)]
    (i -12)))

(defn funcion-complement-4
  []
  (let [fu (fn [x] (number? x))
        i (complement fu)]
    (i "hola")))

(defn funcion-complement-5
  []
  (let [fu (fn [x] (rational? x))
        i (complement fu)]
    (i 0.5)))

(defn funcion-complement-6
  []
  (let [fu (fn [x] (keyword? x))
        i (complement fu)]
    (i :D)))

(defn funcion-complement-7
  []
  (let [hu (fn [xs] (filter number? xs))
        i (comp  hu)
        g (complement i)]
    (g [1 2 3 "q"])))

(defn funcion-complement-8
  []
  (let [fu (fn [x] (symbol? x))
        i (complement fu)]
    (i 'a)))

(defn funcion-complement-9
  []
  (let [fu (fn [x] (float? x))
        i (complement fu)]
    (i 10.9)))

(defn funcion-complement-10
  []
  (let [fu (fn [x] (integer? x))
        i (complement fu)]
    (i 1000)))

(defn funcion-complement-11
  []
  (let [fu (fn [x] (decimal? x))
        i (complement fu)]
    (i 109M)))

(defn funcion-complement-12
  []
  (let [fu (fn [x] (neg-int? x))
        i (complement fu)]
    (i -1)))

(defn funcion-complement-13
  []
  (let [fu (fn [x] (ident? x))
        i (complement fu)]
    (i :Hello)))

(defn funcion-complement-14
  []
  (let [fu (fn [x] (true? x))
        i (complement fu)]
    (i 10)))

(defn funcion-complement-15
  []
  (let [fu (fn [x] (false? x))
        i (complement fu)]
    (i true)))

(defn funcion-complement-16
  []
  (let [hu (fn [xs] (filter char? xs))
        i (comp  hu)
        g (complement i)]
    (g [\q \w \s])))

(defn funcion-complement-17
  []
  (let [hu (fn [xs] (filter keyword? xs))
        i (comp  hu)
        g (complement i)]
    (g [\q \w \s :a :qwe])))

(defn funcion-complement-18
  []
  (let [hu (fn [xs] (filter char? xs))
        i (comp  hu)
        g (complement i)]
    (g [\q \w \s])))

(defn funcion-complement-19
  []
  (let [hu (fn [xs] (filter neg-int? xs))
        i (comp  hu)
        g (complement i)]
    (g [2 -4 9 -23 -4])))

(defn funcion-complement-20
  []
  (let [fu (fn [x] (zero? x))
        i (complement fu)]
    (i 2)))

(defn funcion-constantly-1
  []
  (let [fu (fn [xs] (filter number? xs))
        i(constantly 1)]
    (i [1 2 3 \q])))

(defn funcion-constantly-2
  []
  (let [fu (fn [xs] (filter even? xs))
        hu(fn [xs] (map inc xs))
        i (constantly 10)]
    (i [1 2 3 \q])))

(defn funcion-constantly-3
  []
  (let [f [10 20 30]
        g (constantly f)]
    (g 2 3 \q)))

(defn funcion-constantly-3
  []
  (let [f [10 20 30]
        g (constantly f)]
    (g 2 3 \q)))

(defn funcion-constantly-4
  []
  (let [f [\q 20 :hello]
        z (constantly f)]
    (z 2 3 "12")))

(defn funcion-constantly-5
  []
  (let [f {:a 1 :b 2 :c 2}
        z (constantly f)]
    (z {:1 2 :3 4 :4 5})))

(defn funcion-constantly-6
  []
  (let [f ["hola" 99 'a :f \q]
        z (constantly f)]
    (z 2 3 \q)))

(defn funcion-constantly-7
  []
  (let [f [[12 3] [:a :b] ["Q" "E"]]
        z (constantly f)]
    (z 9 6 \e "g" )))

(defn funcion-constantly-8
  []
  (let [f [[12 3] {:E 23 :F 34} 3 ["Q" "E"] 100]
        z (constantly f)]
    (z [2.4 5.5 6/6 12])))

(defn funcion-constantly-9
  []
  (let [f [{:a :b} {:c :d} {:e :f}]
        z (constantly f)]
    (z {:a 100})))

(defn funcion-constantly-10
  []
  (let [f #{1 20 40 100 500}
        z (constantly f)]
    (z #{ 90 65 43 2 1})))

(defn funcion-constantly-11
  []
  (let [f '(1 2 3 4 5)
        z (constantly f)]
    (z ["char" \q 'q])))

(defn funcion-constantly-12
  []
  (let [f '([1 2 3 4 5] {:Hello 2 :Valor 3} #{10 20 30})
        z (constantly f)]
    (z ["char" \q 'q] '(1 4 6 8))))

(defn funcion-constantly-13
  []
  (let [f '("Manuel" "Rodolfo" "Javier" "Adan")
        z (constantly f)]
    (z ["char" \q 'q] '(1 4 6 8))))

(defn funcion-constantly-14
  []
  (let [f (+ 2 3 4 9)
        z (constantly f)]
    (z * 2 3)))

(defn funcion-constantly-15
  []
  (let [f (* 200 4)
        z (constantly f)]
    (z [3 4 67])))

(defn funcion-constantly-16
  []
  (let [f (map even? [2 3 4 5 6 7])
        z (constantly f)]
    (z ["juan" "jose" "mario"])))

(defn funcion-constantly-17
  []
  (let [f (filter char? [\q 'q "q" :q \w \d])
        z (constantly f)]
    (z  100)))

(defn funcion-constantly-18
  []
  (let [f (vector (filter neg-int? [-3 4 -100 \q -87 :d]))
        z (constantly f)]
    (z [22 34] [12 0] {:A 1})))

(defn funcion-constantly-19
  []
  (let [f (count [10 \q :d 23 "gola"])
        z (constantly f)]
    (z :a [1 2 3])))

(defn funcion-constantly-20
  []
  (let [f (filter odd? [1 2 3 4 5 6 7 8 9])
        z (constantly f)]
    (z [:a 2 :g 34 :k 89 :l 0])))


(defn funcion-every-pred-1
  []
  (let [f (fn [xs] (filter odd? xs))
        z (every-pred f)]
    (z [3 9 7])))

(defn funcion-every-pred-2
  []
  (let [f (fn [xs] (even? xs))
        z (every-pred f)]
    (z 2 3 \q 9 7)))

(defn funcion-every-pred-3
  []
  (let [f (fn [xs] (char? xs))
        z (every-pred f)]
    (z \q \s \q \a )))

(defn funcion-every-pred-4
  []
  (let [f (fn [xs] (pos? xs))
        z (every-pred f)]
    (z 12 -3 4 -90)))

(defn funcion-every-pred-5
  []
  (let [f (fn [xs] (number? xs))
        z (every-pred f)]
    (z 1 2 :a 3 4 \q)))

(defn funcion-every-pred-6
  []
  (let [f (fn [xs] (rational? xs))
        z (every-pred f)]
    (z 1 2 3 4 5 6)))

(defn funcion-every-pred-7
  []
  (let [f (fn [xs] (keyword? xs))
        z (every-pred f)]
    (z :1 :2 :3 4 5 6)))

(defn funcion-every-pred-8
  []
  (let [f (fn [xs] (symbol? xs))
        z (every-pred f)]
    (z 'q 'w 'e 'r 'q)))

(defn funcion-every-pred-9
  []
  (let [f (fn [xs] (float? xs))
        z (every-pred f)]
    (z 1.0 0.3 4N)))

(defn funcion-every-pred-10
  []
  (let [f (fn [xs] (neg-int? xs))
        z (every-pred f)]
    (z -2 -3.4 -1 -100.40)))

(defn funcion-every-pred-11
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (filter neg-int? xs))
        z (every-pred f g)]
    (z 1 23 -2 -1)))

(defn funcion-every-pred-12
  []
  (let [f (fn [xs] (filter char? xs))
        z ( every-pred f)]
    (z [\q \w \e \r 1])))

(defn funcion-every-pred-13
  []
  (let [f (fn [xs] (float? xs))
        g (fn [xs] (number? xs))
        z (every-pred f g)]
    (z 1.3 3.5 4.4)))

(defn funcion-every-pred-14
  []
  (let [f (fn [xs] (neg-int? xs))
        g (fn [xs] (number? xs))
        z (every-pred f g)]
    (z -1 -3 -5 -6)))

(defn funcion-every-pred-15
  []
  (let [f (fn [xs] (seq? xs))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z [1 2 3 4])))

(defn funcion-every-pred-16
  []
  (let [f (fn [xs] (seq? xs))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z '(1 2 3 4))))

(defn funcion-every-pred-17
  []
  (let [l (fn [xs] (char? (first xs)))
        f (fn [xs] (seq? xs))
        g (fn [xs] (list? xs))
        z (every-pred l f g)]
    (z '(\q \w \e))))

(defn funcion-every-pred-18
  []
  (let [f (fn [xs] (keyword? (first xs)))
        g (fn [xs] (list? xs))
        z (every-pred f g)]
    (z '(:a  :b 2 :e  :c 23))))

(defn funcion-every-pred-19
  []
  (let [f (fn [xs] (pos-int? (first xs)))
        g (fn [xs] (filter number? xs))
        h (fn [xs] (vector? xs))
        z (every-pred f g h)]
    (z [1 2 -3 -12 \q :d])))

(defn funcion-every-pred-20
  []
  (let [f (fn [xs] (even? (first xs)))
        g (fn [xs] (filter number? xs))
        h (fn [xs] (vector? xs))
        z (every-pred f g h)]
    (z [1 2 :a 4 \q 8 10 12 24])))

(defn funcion-fnil-1
  []
  (let [f (fn [x] (> x 10))
        g (fn [x] (< x 11))
        z (fnil f g)]
    (z 30)))

(defn funcion-fnil-2
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f 7)]
    (z nil 5)))

(defn funcion-fnil-3
  []
  (let [f (fn [x y] (+ x y))
        z (fnil f 34 4)]
    (z nil nil)))

(defn funcion-fnil-4
  []
  (let [f (fn [x y] (- x y))
        z (fnil f 34 4)]
    (z nil nil)))

(defn funcion-fnil-5
  []
  (let [f (fn [x y] (+ (* x y) (* y x)))
        z (fnil f 2)]
    (z nil 3)))

(defn funcion-fnil-6
  []
  (let [f (fn [x y] (+ (* 2 x) (* 3 y)))
        z (fnil f 2)]
    (z nil 3)))

(defn funcion-fnil-7
  []
  (let [f (fn [x y a] (+ (* 2 x) (* 3 y) (* 4 a)))
        z (fnil f 5)]
    (z nil 5 5)))
